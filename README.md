# Dotfiles

### Setting up new computer:

```bash
echo ".cfg" >> .gitignore
```

There could be weird behaviour if .cfg tries to track itself. Avoid recursive issues by adding .cfg to your global Git ignore.

```bash
git clone https://gitlab.com/taylorosbourne/dotfiles.git $HOME/.cfg --bare
```

Add a --bare flag if you wish you use a bare repo.

```bash
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.config/fish/config.fish
```

Run config status and you should see a list of untracked files in the $HOME dire

```bash
config config --local status.showUntrackedFiles no
```

Run config status again, and you’ll get the message On branch main nothing to commit

```bash
config checkout
```
