set nocompatible

if has('filetype')
  filetype indent plugin on
endif

syntax on

set encoding=utf-8
set hidden
set wildmenu
set showcmd
set hlsearch
set ignorecase
set smartcase
set backspace=indent,eol,start
set autoindent
set nostartofline
set ruler
set laststatus=2
set confirm
set visualbell
set t_vb=

set cmdheight=2

set number
