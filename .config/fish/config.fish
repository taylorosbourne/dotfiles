rvm default
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.crystal/bin:$PATH"
export PATH="$HOME/.deno/bin:$PATH"
export PATH="$HOME/.go/bin:$PATH"
export PATH="$HOME/.ruby/bin:$PATH"
export PATH="$HOME/golib/bin:$PATH"
export NVM_DIR="$HOME/.nvm"

starship init fish | source

alias config='/usr/bin/git --git-dir=/Users/taylorosbourne/.cfg/ --work-tree=/Users/taylorosbourne'
