function oweb
	if test $argv[2]
		if test $argv[2] = "--http"
			open http://$argv[1]
        	else    
            		echo "Unknown: $argv[2]. Try adding --http to open web with http. Default is https."
        	end
	else
		open https://$argv
	end
end
